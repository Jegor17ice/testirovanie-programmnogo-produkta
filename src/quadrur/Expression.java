package quadrur;

public class Expression {
    private double a;
    private double b;
    private double c;

    public Expression(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;

    }

    public double[] countExpression() throws NegativeDiscriminantException {
        double discriminant = countDiscriminant();
        double[] roots = new double[2];
        roots[0] = countRoot(discriminant);
        if (discriminant == 0) {
            roots[1] = countRoot(-discriminant);
        }
        return roots;
    }

    public double countDiscriminant() throws NegativeDiscriminantException {
        double discriminant = b*b - 4 * a * c;
        if (discriminant < 0) {
            throw new NegativeDiscriminantException();
        }
        return discriminant;
    }

    public double countRoot(double discriminant) {
        return -b + Math.sqrt(discriminant) / 2 * a;

    }
}
