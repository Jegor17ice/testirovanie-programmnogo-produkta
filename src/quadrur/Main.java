package quadrur;

import triangle.NeativeValueException;
import triangle.Triangle;
import triangle.UncorrectValuesException;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Expression expression = null;
        System.out.println("Программа для решения уравнения вида: ax 2 + bx + c = 0");
        System.out.println("Введите значения a b и c");
        try (Scanner sc = new Scanner(System.in)) {
            expression = new Expression(sc.nextInt(), sc.nextInt(), sc.nextInt());
            double[] roots = expression.countExpression();
            System.out.print(" Корни уравнения : " + roots[0] + "   "+roots[1] );
        }
        catch (Exception e ){
            System.out.println("Ошибка ввода");
        } catch (NegativeDiscriminantException e) {
            System.out.println("Дискриминант отрицателен");
        }

    }
}
