package triangle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Triangle triangle = null;
        System.out.println("Введите стороны треугольника :");
        try(Scanner sc = new Scanner(System.in)) {
            triangle = new Triangle(sc.nextInt(), sc.nextInt(), sc.nextInt());
        } catch (UncorrectValuesException e) {
            System.out.println("треугольник неправильный");
        } catch (NeativeValueException e) {
            System.out.println("введены отрицательные значения");
        }

        System.out.println(triangle.typeOfTriangle());

    }
}
