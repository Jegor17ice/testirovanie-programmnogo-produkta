package triangle;

import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class Triangle {
    private int a;
    private int b;
    private int c;

    public Triangle(int a, int b, int c) throws UncorrectValuesException, NeativeValueException {
        this.a = a;
        this.b = b;
        this.c = c;
        verefication();
    }

    public String typeOfTriangle() {
        String message = "треугольник";
        if (isIsosceles()) {
            message = " равнобедренный,";
        }
        if (isEquilateral()) {
            message = message + " равносторонний";
        } else {
            message = message + " не равносторонний";
        }
        if (isRectangular()) {
            message = message + " и прямоугольный";
        } else {
            message = message + " и не прямоугольный";
        }
        return message;
    }

    public boolean isRectangular() { //прямоугольный
        return (a * a == b * b + c * c || b * b == c * c + a * a || c * c == b * b + a * a);
    }

    public void verefication() throws NeativeValueException, ValueException, UncorrectValuesException {
        if (a <= 0 || b <= 0 || c <= 0) {
            throw new NeativeValueException();
        }
        if (a + b < c || a + c < b || b + c < a) {
            throw new UncorrectValuesException();
        }
    }

    public boolean isIsosceles() {
        return (a == b || b == c || c == a);
    } //равнобедренный

    public boolean isEquilateral() {
        return (a == b && b == c);
    } //равносторонний
}
