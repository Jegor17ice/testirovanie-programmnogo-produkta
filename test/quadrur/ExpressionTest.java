package quadrur;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExpressionTest {

    @Test
    public void countExpression() throws NegativeDiscriminantException {
        Expression expression = new Expression(3, 14, 5);
        double[] actual = {3.492855684535904, 0};
        assertArrayEquals(expression.countExpression(), actual, 0);
    }

    @Test
    public void countDiscriminant() throws NegativeDiscriminantException {
        Expression expression = new Expression(3, 14, 5);
        expression.countDiscriminant();
    }

    @Test(expected = NegativeDiscriminantException.class)
    public void countNegativeDiscriminant() throws NegativeDiscriminantException {
        Expression expression = new Expression(1, 2, 3);
        expression.countDiscriminant();
    }

    @Test
    public void countRoot() throws NegativeDiscriminantException {
        Expression expression = new Expression(3, 14, 5);
        long actual = (long) expression.countRoot(expression.countDiscriminant());
        long expected = (long) 3.492855684535904;
        assertEquals(expected, actual);
    }

}