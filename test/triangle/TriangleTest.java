package triangle;

import org.junit.Test;
import triangle.NeativeValueException;
import triangle.Triangle;
import triangle.UncorrectValuesException;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test(expected = NeativeValueException.class)
    public void vereficationNeativeValue() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(-1, 23, 3);
    }

    @Test(expected = UncorrectValuesException.class)
    public void verefication() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(1, 23, 3);

    }

    @Test
    public void isIsosceles() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(22, 22, 3);
        assertTrue(triangle.isIsosceles());
    }

    @Test
    public void isEquilateral() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(22, 22, 22);
        assertTrue(triangle.isIsosceles());
    }

    @Test
    public void isIsoscelesException() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(21, 22, 3);
        assertFalse(triangle.isIsosceles());
    }

    @Test
    public void isEquilateralException() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(2, 22, 22);
        assertFalse(triangle.isEquilateral());
    }
    @Test
    public void isRectangular() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(3, 4, 5);
        assertTrue(triangle.isRectangular());
    }
    @Test()
    public void isRectangular2() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(5, 12, 13);
        assertTrue(triangle.isRectangular());
    }
    @Test()
    public void isRectangularException() throws NeativeValueException, UncorrectValuesException {
        Triangle triangle = new Triangle(5, 12, 11);
        assertFalse(triangle.isRectangular());
    }
}